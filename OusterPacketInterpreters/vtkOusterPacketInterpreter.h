/*=========================================================================

  Program:   LidarView
  Module:    vtkOusterPacketInterpreter.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkOusterPacketInterpreter_h
#define vtkOusterPacketInterpreter_h

#include <vtkLidarPacketInterpreter.h>

#include <memory>

#include "OusterPacketInterpretersModule.h"

class OUSTERPACKETINTERPRETERS_EXPORT vtkOusterPacketInterpreter : public vtkLidarPacketInterpreter
{
public:
  static vtkOusterPacketInterpreter* New();
  vtkTypeMacro(vtkOusterPacketInterpreter, vtkLidarPacketInterpreter);
  void PrintSelf(ostream& vtkNotUsed(os), vtkIndent vtkNotUsed(indent)) override{};

  /**
   * Initializes the lidar calibration or configuration.
   * This method is called during interpreter initialization, whenever the address/port is changed,
   * or when ResetInitializedState() is called.
   */
  void Initialize() override;

  /**
   * Checks if the current packet is valid for the selected lidar model.
   * Return false when invalid. Invalid packets will be skipped.
   */
  bool IsLidarPacket(unsigned char const* data, unsigned int dataLength) override;

  /**
   * Builds a frame index for random access when reading a pcap file.
   * Returns true when a new frame is detected.
   *
   * Note that the first packet of the new frame contains info of the previous packet.
   */
  bool PreProcessPacket(unsigned char const* data,
    unsigned int dataLength,
    double& outLidarDataTime) override;

  /**
   * Processes the packet, filling point information using packet data,
   * and calling SplitFrame when necessary.
   */
  void ProcessPacket(unsigned char const* data, unsigned int dataLength) override;

protected:
  /**
   * Creates a new empty frame object, which will be filled by ProcessPacket.
   */
  vtkSmartPointer<vtkPolyData> CreateNewEmptyFrame(vtkIdType nbrOfPoints,
    vtkIdType prereservedNbrOfPoints = 60000) override;

  vtkOusterPacketInterpreter();
  ~vtkOusterPacketInterpreter();

private:
  vtkOusterPacketInterpreter(const vtkOusterPacketInterpreter&) = delete;
  void operator=(const vtkOusterPacketInterpreter&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif // vtkOusterPacketInterpreter_h
