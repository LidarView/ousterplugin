/*=========================================================================

  Program:   LidarView
  Module:    vtkOusterPacketInterpreter.cxx

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkOusterPacketInterpreter.h"
#include "InterpreterHelper.h"

#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkShortArray.h>
#include <vtkTypeUInt64Array.h>
#include <vtkUnsignedIntArray.h>

#include <memory>

//-----------------------------------------------------------------------------
class vtkOusterPacketInterpreter::vtkInternals
{
public:
  vtkSmartPointer<vtkPoints> Points;
  vtkSmartPointer<vtkDoubleArray> PointsX;
  vtkSmartPointer<vtkDoubleArray> PointsY;
  vtkSmartPointer<vtkDoubleArray> PointsZ;
  vtkSmartPointer<vtkUnsignedCharArray> Intensity;
  vtkSmartPointer<vtkUnsignedCharArray> LaserId;
  vtkSmartPointer<vtkDoubleArray> Distance;
  vtkSmartPointer<vtkDoubleArray> Timestamp;
  vtkSmartPointer<vtkDoubleArray> Azimuth;
};

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkOusterPacketInterpreter)

//-----------------------------------------------------------------------------
vtkOusterPacketInterpreter::vtkOusterPacketInterpreter()
  : Internals(new vtkOusterPacketInterpreter::vtkInternals())
{
  // Theses information will be stored in point cloud field data
  this->SetSensorVendor("Ouster");

  this->ResetCurrentFrame();
}

//-----------------------------------------------------------------------------
vtkOusterPacketInterpreter::~vtkOusterPacketInterpreter() = default;

//-----------------------------------------------------------------------------
void vtkOusterPacketInterpreter::Initialize()
{
  Superclass::Initialize();
}

//-----------------------------------------------------------------------------
void vtkOusterPacketInterpreter::ProcessPacket(unsigned char const* data, unsigned int dataLength)
{
  // Insert a point
  // internals->Points->InsertNextPoint(pos);
  // InsertNextValueIfNotNull(internals->PointsX, pos[0]);
  // InsertNextValueIfNotNull(internals->PointsY, pos[1]);
  // InsertNextValueIfNotNull(internals->PointsZ, pos[2]);
  // InsertNextValueIfNotNull(internals->Timestamp, currentTimestamp);
  // InsertNextValueIfNotNull(internals->Intensity, intensity);
  // InsertNextValueIfNotNull(internals->LaserId, laserId);
  // InsertNextValueIfNotNull(internals->Distance, distance);
  // InsertNextValueIfNotNull(internals->Azimuth, currentAzimuth);
}

//-----------------------------------------------------------------------------
bool vtkOusterPacketInterpreter::IsLidarPacket(unsigned char const* data, unsigned int dataLength)
{
  return false;
}

//-----------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> vtkOusterPacketInterpreter::CreateNewEmptyFrame(vtkIdType nbrOfPoints,
  vtkIdType prereservedNbrOfPoints)
{
  const int defaultPrereservedNbrOfPointsPerFrame = 60000;
  // prereserve for 50% points more than actually received in previous frame
  prereservedNbrOfPoints =
    std::max(static_cast<int>(prereservedNbrOfPoints * 1.5), defaultPrereservedNbrOfPointsPerFrame);

  vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

  // Initialize points
  vtkNew<vtkPoints> points;
  points->SetDataTypeToFloat();
  points->Allocate(prereservedNbrOfPoints);
  if (nbrOfPoints > 0)
  {
    points->SetNumberOfPoints(nbrOfPoints);
  }
  points->GetData()->SetName("Points");
  polyData->SetPoints(points.GetPointer());

  auto& internals = this->Internals;
  internals->Points = points.GetPointer();
  // clang-format off
  InitArrayForPolyData(true, internals->PointsX, "X", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, internals->PointsY, "Y", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, internals->PointsZ, "Z", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(false, internals->Intensity, "intensity", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->LaserId, "laser_id", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Timestamp, "timestamp", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Distance, "distance_m", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Azimuth, "azimuth", nbrOfPoints, prereservedNbrOfPoints, polyData);
  // clang-format on

  // Set the default array to display in the application
  polyData->GetPointData()->SetActiveScalars("intensity");
  return polyData;
}

//-----------------------------------------------------------------------------
bool vtkOusterPacketInterpreter::PreProcessPacket(unsigned char const* data,
  unsigned int dataLength,
  double& outLidarDataTime)
{
  // Must return true if we split the frame at the current packet
  return false;
}
